import { NIL as EmptyUUID  } from 'uuid';
import {Tag} from './tag';

export interface Word {
  id: string;
  name: string;
  definition: string;
  tags: Tag[];
}

export interface Words {
  words: Array<Word>;
}

export const EmptyWords: Words = { words: new Array<Word>()};

export const EmptyWord: Word = {
  id: EmptyUUID, name: '', definition: '', tags: new Array<Tag>()
};
