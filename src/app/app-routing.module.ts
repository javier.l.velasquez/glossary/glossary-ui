import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';

import {WordsComponent} from './words/words.component';
import {ShellService} from './shell/shell.service';

const routes: Routes = [
  ShellService.childRoutes([
    {
      path: 'define-word',
      loadChildren: () => import('./word-form/word-form.module').then(m => m.WordFormModule)
    },
    {
      path: 'search-results',
      loadChildren: () => import('./words/words.module').then(m => m.WordsModule)
    }
  ]),
  { path: '', pathMatch: 'full', component: WordsComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
