import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import { Observable} from 'rxjs';
import { catchError} from 'rxjs/operators';
import {Word, Words} from './word';

@Injectable({
  providedIn: 'root'
})
export class GlossaryService {
  private baseUrl = 'http://localhost:8080/glossary';

  constructor(private http: HttpClient) { }

  random(offset: number = 0): Observable<Words> {
    return this.search('', offset);
  }

  define(word: Word): Observable<Word[]> {
    return this.http.post<Word[]>(this.baseUrl, word, {});
  }

  search(term: string, offset: number = 0): Observable<Words> {
    const options = { params: new HttpParams().set('limit', '6').set('offset', offset.toString()).set('q', term) } ;

    return this.http.get<Words>(this.baseUrl, options)
      .pipe(
        catchError(() => new Observable<Words>(subscriber => {
            subscriber.next({words: new Array<Word>()});
          })
        ));
  }

}

