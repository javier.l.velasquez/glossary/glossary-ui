import { Injectable } from '@angular/core';
import {Word, Words} from './word';
import {Subject, Observable} from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class WordsService {
  private words: Subject<Words> = new Subject<Words>();

  constructor() {
  }

  subscribe(f: (w: Words) => void): void {
    this.words.subscribe({next: (a: Words) => f(a)});
  }

  publish(words: Words): void {
    this.words.next(words);
  }
}
