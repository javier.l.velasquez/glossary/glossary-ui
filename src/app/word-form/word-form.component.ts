import {Component, Inject, OnInit} from '@angular/core';
import {MatChipInputEvent} from '@angular/material/chips';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {FormBuilder, FormControl, Validators} from '@angular/forms';
import {GlossaryService} from '../glossary.service';
import {NIL as EmptyUUID} from 'uuid';
import {Router} from '@angular/router';
import {TagService} from '../tag.service';
import {Tag, EmptyTag} from '../tag';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {EmptyWord} from '../word';

@Component({
  selector: 'app-word-form',
  templateUrl: './word-form.component.html',
  styleUrls: ['./word-form.component.sass']
})
export class WordFormComponent implements OnInit {

  valid = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  validatedTags: ValidatedTag[] = [];
  wordForm = this.formBuilder.group({
    name: new FormControl('', [Validators.required, Validators.minLength(3)]),
    definition: new FormControl('', [Validators.required, Validators.minLength(25)]),
    tags: new FormControl('', [])
  });


  getFormErrors(): string {
    // make this more robust
    const errorCodes = new Array<string>('required', 'minlength');
    const errors = errorCodes.map(errorCode => {
      let messages = '';
      if (this.wordForm.get('name')?.hasError(errorCode)) {
        messages = 'name failed ' + errorCode + ' validation';
      }
      if (this.wordForm.get('definition')?.hasError(errorCode)) {
        if (messages.length > 0) {
          messages = messages + ' and definition failed ' + errorCode + ' validation';
        } else {
          messages = 'definition failed ' + errorCode + ' validation';
        }
      }
      return messages;
    }).filter(a => a.length > 0).join(', ');
    return errors === ',' ? '' : errors;
  }

  constructor(
    private glossaryService: GlossaryService,
    private categoryService: TagService,
    private formBuilder: FormBuilder,
    private router: Router,
    public dialog: MatDialog,
    readonly snackBar: MatSnackBar) {
  }

  private displayAddTagForm(tag: ValidatedTag): void {
    const dialogRef = this.dialog.open(DialogOverviewExampleDialogComponent, {
      width: '250px',
      data: {tagName: tag.name}
    });
    dialogRef.afterClosed().subscribe((result: string) => {
      this.categoryService.add({id: EmptyUUID, name: result}).subscribe(category => {
        this.validateDisplayedTagByCategory(category);
      });
    });
  }

  private validateDisplayedTagByCategory(category: Tag): void {
    this.findDisplayedTag(category, displayedTag => {
      displayedTag.validated = Validated;
      displayedTag.id = category.id;
    });
  }

  private findDisplayedTag(category: Tag, f: (tag: ValidatedTag) => void): void {
    const resolvedTag = this.validatedTags.find(t => t.name === category.name);
    if (resolvedTag !== undefined) {
      f(resolvedTag);
    }
  }

  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    if ((value || '').trim()) {
      const name = value.trim();
      const tag: ValidatedTag = {id: '', name, validated: NotValidated};
      this.validatedTags.push(tag);

      this.categoryService.findByName(name).subscribe(category => {
        if (category !== EmptyTag) {
          this.validateDisplayedTagByCategory(category);
        } else {
          tag.validated = FailedValidation;
          this.displayAddTagForm(tag);
        }
      });
    }


    // Reset the input value
    if (input) {
      input.value = '';
    }
  }

  remove(tag: ValidatedTag): void {
    const index = this.validatedTags.indexOf(tag);

    if (index >= 0) {
      this.validatedTags.splice(index, 1);
    }
  }

  ngOnInit(): void {
    this.wordForm.statusChanges.subscribe(status => {
      if (status.toString() === 'INVALID') {
        this.valid = false;
      }
    });
  }

  onSubmit(): void {

    const errorMessage = this.getFormErrors();
    if (errorMessage.length > 0) {
      this.snackBar.open(errorMessage, 'Error');
    } else {
      const unValidatedTags = this.validatedTags.filter(tag => tag.validated !== Validated).map(tag => tag.name).toString();
      if (unValidatedTags.length > 0) {
        if (unValidatedTags.length > 15) {
          this.snackBar.open('the following tags are unvalidated: ' + unValidatedTags.substring(0, 15) + '...', '🚩');
        } else {
          this.snackBar.open('the following tags are unvalidated: ' + unValidatedTags, '🚩');
        }
      } else {
        const name = this.wordForm.get('name')?.value.toString();
        const definition = this.wordForm.get('definition')?.value.toString();

        const tags: Array<Tag> = this.validatedTags.map(validatedTag => {
          if (validatedTag.id.length > 0) {
            return {id: validatedTag.id, name: validatedTag.name};
          } else {
            return EmptyWord;
          }
        }).filter(t => t !== EmptyTag);

        this.glossaryService.define(
          {
            id: EmptyUUID, name, definition, tags
          }
        ).subscribe(
          () => {
            this.snackBar.open(JSON.stringify('Added word!'), '🎉');
            this.router.navigate(['/']);
          },
          error => this.snackBar.open(JSON.stringify(error.error.message.Message), 'Error'),
        );
      }

    }


  }
}


@Component({
  selector: 'app-dialog-overview-example-dialog',
  templateUrl: 'dialog-overview-example-dialog.html',
})
export class DialogOverviewExampleDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<DialogOverviewExampleDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}


export interface DialogData {
  tagName: string;
}

export interface ValidationState {
  accent: string;
}

export const NotValidated: ValidationState = {accent: 'accent'};
export const Validated: ValidationState = {accent: 'primary'};
export const FailedValidation: ValidationState = {accent: 'warn'};

export interface ValidatedTag {
  id: string;
  name: string;
  validated: ValidationState;
}

