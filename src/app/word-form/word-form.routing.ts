import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {WordFormComponent} from './word-form.component';

const routes: Routes = [
  // Module is lazy loaded, see app-routing.module.ts
  { path: '', component: WordFormComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class WordFormRouting {}
