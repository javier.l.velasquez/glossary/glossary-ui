import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {DialogOverviewExampleDialogComponent, WordFormComponent} from './word-form.component';
import {MAT_FORM_FIELD_DEFAULT_OPTIONS, MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material/select';
import {MatInputModule} from '@angular/material/input';
import {WordFormRouting} from './word-form.routing';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatButtonModule} from '@angular/material/button';
import {MatChipsModule} from '@angular/material/chips';
import {MatIconModule} from '@angular/material/icon';
import {MatDialogModule} from '@angular/material/dialog';

@NgModule({
  imports: [
    CommonModule,
    MatFormFieldModule,
    MatSelectModule,
    MatButtonModule,
    ReactiveFormsModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule,
    MatChipsModule,
    WordFormRouting,
    MatIconModule,
    MatDialogModule,
],
  exports: [
    MatFormFieldModule, MatInputModule
  ],
  declarations: [WordFormComponent, DialogOverviewExampleDialogComponent],
  providers: [
    { provide: MAT_FORM_FIELD_DEFAULT_OPTIONS, useValue: { appearance: 'fill' } },
  ]
})
export class WordFormModule {}
