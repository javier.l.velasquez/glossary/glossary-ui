import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material/select';
import {MatInputModule} from '@angular/material/input';
import {WordsComponent} from './words.component';
import {MatCardModule} from '@angular/material/card';
import {WordCardComponent} from '../word-card/word-card.component';
import {WordsRouting} from './words.routing';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';

@NgModule({
  imports: [
    CommonModule,
    MatSelectModule,
    MatCardModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    WordsRouting,
    MatIconModule,
    MatButtonModule
  ],
  declarations: [WordsComponent, WordCardComponent]
})
export class WordsModule {}
