import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {WordsComponent} from './words.component';
import {ShellService} from '../shell/shell.service';

const routes: Routes = [
  // Module is lazy loaded, see app-routing.module.ts
  ShellService.childRoutes([
    { path: '', redirectTo: '/listing-grid', pathMatch: 'full' },
    { path: 'listing-grid', component: WordsComponent }
  ])
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class WordsRouting {}
