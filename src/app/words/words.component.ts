import {Component, HostListener, Input, OnInit} from '@angular/core';
import {GlossaryService} from '../glossary.service';
import {EmptyWord, Word, Words} from '../word';
import {WordsService} from '../words.service';
import {BehaviorSubject, Observable, Subject} from 'rxjs';


@Component({
  selector: 'app-words',
  templateUrl: './words.component.html',
  styleUrls: ['./words.component.sass']
})
export class WordsComponent {
  words: Word[] = new Array<Word>();
  // position: Subject<number> = new BehaviorSubject<number>(0);
  position = 0;

  constructor(private glossaryService: GlossaryService, wordsService: WordsService) {
    glossaryService.random().subscribe(words => this.words = words.words);

    wordsService.subscribe(words => {
      this.words = words.words;
    });

  }

  @HostListener('window:keyup', ['$event'])
  keyEvent(event: KeyboardEvent) {
    console.log(event);

    if (event.code === 'ArrowDown' || event.code === 'ArrowRight') {
      this.next();
    }

    if (event.code === 'ArrowLeft' || event.code === 'ArrowUp') {
      this.back();
    }
  }


  next(): void {
    this.position = this.position + 5;
    this.glossaryService.random(this.position).subscribe(words => {
      if (!words.words || words.words.length === 0) {
        this.position = this.position - 5;
      } else {
        this.words = words.words;
      }
    });
  }

  back(): void {
    if (this.position >= 5) {
      this.position = this.position - 5;
      this.glossaryService.random(this.position).subscribe(words => this.words = words.words);
    }
  }

}
