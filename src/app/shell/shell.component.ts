import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {Tag} from '../tag';
import {MediaMatcher} from '@angular/cdk/layout';
import {TagService} from '../tag.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-shell',
  templateUrl: './shell.component.html',
  styleUrls: ['./shell.component.sass']
})
export class ShellComponent implements OnInit {
  title = 'glossary-ui';
  mobileQuery: MediaQueryList;
  categories: Tag[] = new Array<Tag>();

  private _mobileQueryListener: () => void;

  constructor(changeDetectorRef: ChangeDetectorRef, media: MediaMatcher, private router: Router, private categoriesClient: TagService) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
  }

  ngOnInit(): void {
    this.categoriesClient.list().subscribe(categories => this.categories = categories);
  }
}
