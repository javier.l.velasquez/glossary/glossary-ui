import { NIL as EmptyUUID  } from 'uuid';

export interface Tag {
  id: string;
  name: string;
}

export const EmptyTag: Tag = {
  id: EmptyUUID, name: 'Not Categorized'
};
