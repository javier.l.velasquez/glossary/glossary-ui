import {Component, Input, OnInit} from '@angular/core';
import {EmptyWord, Word} from '../word';

@Component({
  selector: 'app-word-card',
  templateUrl: './word-card.component.html',
  styleUrls: ['./word-card.component.sass']
})
export class WordCardComponent {
  @Input() word: Word = EmptyWord;
}
