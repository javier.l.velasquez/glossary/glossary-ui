import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {Tag, EmptyTag} from './tag';

@Injectable({
  providedIn: 'root'
})
export class TagService {
  private baseUrl = 'http://localhost:8080/tags';

  constructor(private http: HttpClient) {
  }

  list(): Observable<Tag[]> {
    return this.http.get<Tag[]>(this.baseUrl, {})
      .pipe(
        catchError(() => new Observable<Tag[]>(subscriber => {
            subscriber.next(new Array<Tag>(EmptyTag));
          })
        ));
  }

  findByName(name: string): Observable<Tag> {
    const options = {params: new HttpParams().set('q', name)};
    const baseUrl = 'http://localhost:8080/tag';

    return this.http.get<Tag>(baseUrl, options)
      .pipe(
        catchError(() => new Observable<Tag>(subscriber => {
            subscriber.next(EmptyTag);
          })
        ));

  }

  add(category: Tag): Observable<Tag> {
    return this.http.post<Tag>(this.baseUrl, category, {})
      .pipe(
        catchError(() => new Observable<Tag>(subscriber => {
            subscriber.next(EmptyTag);
          })
        ));
  }
}
