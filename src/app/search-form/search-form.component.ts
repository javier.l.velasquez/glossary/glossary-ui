import {Component, OnInit} from '@angular/core';
import {GlossaryService} from '../glossary.service';
import {WordsService} from '../words.service';

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.sass']
})
export class SearchFormComponent implements OnInit {
  value = '';

  constructor(private glossaryService: GlossaryService, private wordsService: WordsService) {
  }

  ngOnInit(): void {
  }

  search(): void {
    this.glossaryService.search(this.value).subscribe(words => {
      this.wordsService.publish(words);
    });
  }
}
