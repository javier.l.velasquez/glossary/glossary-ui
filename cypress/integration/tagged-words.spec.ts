import {v4 as uuidv4} from 'uuid';

describe('Tagged Words', () => {
  beforeEach(() => {
    cy.visit('http://localhost:4200'); // go to the home page
  });
  const wordName = 'new word - ' + uuidv4();

  it('should be created', () => {
    // get the rocket element and verify that the app name is in it
    cy.get('#add')
      .click();

    cy.get('#name').type(wordName);
    cy.get('#definition').type(uuidv4());
    // make this more dynamic
    cy.get('#tag').type('example{enter}');
    cy.get('#submit').click();
    cy
      .url()
      .should('include', 'listing-grid');
  });
});
