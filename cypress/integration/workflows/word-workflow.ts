import {v4 as uuidv4} from 'uuid';


export function AddWord(): void {
  cy.get('#add')
    .click();
  const wordName = 'new word - ' + uuidv4();

  cy.get('#name').type(wordName);
  cy.get('#definition').type(uuidv4());
  cy.get('#submit').click();
  cy.get('.mat-simple-snackbar').should('be.visible');
  cy.get('.mat-simple-snackbar-action').should('have.text', '🎉');

  cy
    .url()
    .should('include', 'listing-grid');

}

export function AddWords(count: number): void {
  for (let i = 0; i < count; i++) {
    AddWord();
  }

}
