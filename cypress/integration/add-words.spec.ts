/// <reference types="Cypress" />
import {v4 as uuidv4} from 'uuid';
import {AddWord} from './workflows/word-workflow';


describe('Add Words', () => {
  beforeEach(() => {
    cy.visit('http://localhost:4200'); // go to the home page
  });
  const wordName = 'new word - ' + uuidv4();

  it('should save new words to the glossary', () => {
    // get the rocket element and verify that the app name is in it
    AddWord();
  });
  it('should prevent submission on an empty word', () => {
    cy.get('#add')
      .click();

    cy.get('#definition').type(uuidv4());
    cy.get('#submit').click();

    cy.get('.mat-simple-snackbar').should('be.visible');
    cy.get('.mat-simple-snackbar-action').should('have.text', 'Error');
    cy
      .url()
      .should('include', 'define-word');

  });
  it('should prevent submission on an empty definition', () => {
    cy.get('#add')
      .click();

    cy.get('#name').type(uuidv4());
    cy.get('#submit').click();

    cy.get('.mat-simple-snackbar').should('be.visible');
    cy.get('.mat-simple-snackbar-action').should('have.text', 'Error');
    cy
      .url()
      .should('include', 'define-word');

  });
  it('should inform the user when our word is not successfully submitted', () => {
    cy.get('#add')
      .click();

    cy.get('#name').type(wordName);
    cy.get('#definition').type(uuidv4());
    cy.get('#submit').click();

    cy.get('.mat-simple-snackbar').should('be.visible');
    cy.get('.mat-simple-snackbar-action').should('have.text', 'Error');
    cy
      .url()
      .should('include', 'define-word');

  });
});
