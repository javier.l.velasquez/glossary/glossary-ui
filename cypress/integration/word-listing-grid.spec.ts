/// <reference types="Cypress" />
import {v4 as uuidv4} from 'uuid';
import {AddWords} from './workflows/word-workflow';


describe('Word Listing Grid', () => {
  beforeEach(() => {
    cy.visit('http://localhost:4200'); // go to the home page
  });

  it('should save new words to the glossary', () => {
    AddWords(6);
    cy.get('#next').click();
    // TODO add validation that the pagination has occured
  });
});
